<?php

try {
    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__, null, false);
    $dotenv->load();
}catch (Throwable $t){
    // Ignore load error, fallback to default values.
}