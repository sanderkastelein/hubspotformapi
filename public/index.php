<?php

use Sterc\HubSpot\FormApi\HubSpotApi;

require_once __DIR__ . "/../vendor/autoload.php";

/**
 * Api object used for fetching data from HubSpot
 */
$api = new HubSpotApi(
    getenv('HUBSPOT_API_TOKEN')
);
$forms = $api->getFormIndexCollection();
?>
<!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>HubSpot API</title>
</head>
<body style="height: 100%;">
<header>
    <label for="form-select">Forms:</label>
    <select name="form-select" id="form-select">
        <?php foreach ($forms as $form): ?>
            <option value="<?php echo $form->getGuid(); ?>||<?php echo $form->getPortalId(); ?>">
                <?php echo $form->getName(); ?>
            </option>
        <?php endforeach; ?>
    </select>
    <hr>
</header>
<iframe src="form.php?formId=<?php echo $forms[0]->getGuid(); ?>&portalId=<?php echo $forms[0]->getPortalId(); ?>"
        style="margin:0;padding:0;height:90vh;width:100%;border: none;" id="form"></iframe>
<script>
    (function () {
        window.onload = function () {
            var $form = document.getElementById('form');
            document.getElementById('form-select').onchange = function () {
                var formId = this.value.split('||')[0];
                var portalId = this.value.split('||')[1];

                $form.src = "form.php?formId=" + formId + '&portalId=' + portalId;
            }
        }
    })();
</script>
</body>
</html>
