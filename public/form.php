<?php
use Sterc\HubSpot\FormApi\HubSpotApi;require_once __DIR__ . "/../vendor/autoload.php";


$formGuid = $_GET['formId'];
$portalId = $_GET['portalId'];

$api = new HubSpotApi(
    getenv('HUBSPOT_API_TOKEN')
);

$formHtml = $api->getFormHtmlByPortalIdAndGuid($portalId, $formGuid);

?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title></title>
</head>
<body>
<section>
    <?php
    echo $formHtml; ?>
    <hr>
    <h2>Embed code:</h2>
    <pre><?php echo htmlspecialchars($formHtml); ?></pre>
</section>
</body>
</html>