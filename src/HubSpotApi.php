<?php


namespace Sterc\HubSpot\FormApi;


use GuzzleHttp\Client;
use Sterc\HubSpot\FormApi\Entities\Form;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class HubSpotApi
{
    /**
     * @var string
     */
    private $apiToken;

    /**
     * @var Client with configuration for HubSpot
     */
    private $client;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * HubSpotApi constructor.
     * @param string|null $apiToken
     */
    public function __construct(string $apiToken)
    {
        $this->apiToken = $apiToken;

        $this->serializer = new Serializer(
            [new ObjectNormalizer(), new ArrayDenormalizer()],
            ['json' => new JsonEncoder()]
        );

        $this->client = new Client([
            'base_uri' => getenv('HUBSPOT_ENDPOINT_URI') ?: 'https://api.hubapi.com/',
            'timeout' => getenv('HUBSPOT_REQUEST_TIMEOUT') ?: 20,
            'query' => [
                'hapikey' => $apiToken
            ]
        ]);
    }


    /**
     * @return Form[]
     */
    public function getFormIndexCollection(): array
    {
        $response = $this->client->get('forms/v2/forms');

        return $this->serializer->deserialize(
            $response->getBody(), Form::class . '[]', 'json' // Add [] to ::class to make array of objects
        );
    }

    /**
     * @param string $guid
     * @return Form|null
     */
    public function getFormByGuid(string $guid): ?Form
    {
        $response = $this->client->get('forms/v2/forms/' . $guid);
        $form = $this->serializer->deserialize(
            $response->getBody(), Form::class, 'json'
        );
        if ($form instanceof Form) return $form;
    }

    /**
     * @param Form $form
     * @return string
     */
    public function getFormHtmlForForm(Form $form): string
    {
        $portalId = $form->getPortalId();
        $formId = $form->getGuid();
        return $this->getFormHtmlByPortalIdAndGuid($portalId, $formId);
    }

    /**
     * @param string $portalId
     * @param string $formId
     * @return string
     */
    public function getFormHtmlByPortalIdAndGuid(string $portalId, string $formId): string
    {
        // Note -> move to template file?.
        return <<<EOD
<script charset="utf-8" type="text/javascript" src="https://js.hsforms.net/forms/v2.js"></script>
<script>
  hbspt.forms.create({
    portalId: "$portalId",
    formId: "$formId"
});
</script>
EOD;
    }

}