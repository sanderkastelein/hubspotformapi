<?php


namespace Sterc\HubSpot\FormApi\Entities;


class Form
{
    /**
     * @var int
     */
    private $portalId;
    /**
     * @var string
     */
    private $guid;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $action;
    /**
     * @var string
     */
    private $method;
    /**
     * @var string
     */
    private $cssClass;
    /**
     * @var string
     */
    private $redirect;
    /**
     * @var string
     */
    private $submitText;
    /**
     * @var string
     */
    private $followUpId;
    /**
     * @var string
     */
    private $notifyRecipients;
    /**
     * @var string
     */
    private $leadNurturingCampaignId;
    /**
     * @var array
     */
    private $formFieldGroups;
    /**
     * @var int
     */
    private $createdAt;
    /**
     * @var int
     */
    private $updatedAt;
    /**
     * @var string
     */
    private $performableHtml;
    /**
     * @var string
     */
    private $migratedFrom;
    /**
     * @var bool
     */
    private $ignoreCurrentValues;
    /**
     * @var array
     */
    private $metaData;
    /**
     * @var bool
     */
    private $deletable;
    /**
     * @var string
     */
    private $inlineMessage;
    /**
     * @var string
     */
    private $tmsId;
    /**
     * @var bool
     */
    private $captchaEnabled;
    /**
     * @var string
     */
    private $campaignGuid;
    /**
     * @var bool
     */
    private $cloneable;
    /**
     * @var bool
     */
    private $editable;
    /**
     * @var string
     */
    private $formType;
    /**
     * @var int
     */
    private $deletedAt;
    /**
     * @var string
     */
    private $themeName;
    /**
     * @var int
     */
    private $parentId;
    /**
     * @var string
     */
    private $style;
    /**
     * @var bool
     */
    private $isPublished;
    /**
     * @var int
     */
    private $publishAt;
    /**
     * @var int
     */
    private $unpublishAt;
    /**
     * @var object
     */
    private $multivariateTest;
    /**
     * @var int
     */
    private $kickbackEmailWorkflowId;
    /**
     * @var string
     */
    private $kickbackEmailsJson;
    /**
     * @var string
     */
    private $customUid;
    /**
     * @var bool
     */
    private $createMarketableContact;

    /**
     * Form constructor.
     * Fields are copied from https://developers.hubspot.com/docs/methods/forms/forms_overview
     * @param int $portalId
     * @param string $guid
     * @param string $name
     * @param string $action
     * @param string $method
     * @param string $cssClass
     * @param string $redirect
     * @param string $submitText
     * @param string $followUpId
     * @param string $notifyRecipients
     * @param string $leadNurturingCampaignId
     * @param array $formFieldGroups
     * @param int $createdAt
     * @param int $updatedAt
     * @param string $performableHtml
     * @param string $migratedFrom
     * @param bool $ignoreCurrentValues
     * @param array $metaData
     * @param bool $deletable
     * @param string $inlineMessage
     * @param string $tmsId
     * @param bool $captchaEnabled
     * @param string $campaignGuid
     * @param bool $cloneable
     * @param bool $editable
     * @param string $formType
     * @param int $deletedAt
     * @param string $themeName
     * @param int $parentId
     * @param string $style
     * @param bool $isPublished
     * @param int $publishAt
     * @param int $unpublishAt
     * @param array $multivariateTest
     * @param int $kickbackEmailWorkflowId
     * @param string $kickbackEmailsJson
     * @param string $customUid
     * @param bool $createMarketableContact
     */
    public function __construct(
        int $portalId,
        string $guid,
        string $name,
        string $action,
        string $method,
        string $cssClass,
        string $redirect,
        string $submitText,
        string $followUpId,
        string $notifyRecipients,
        string $leadNurturingCampaignId,
        array $formFieldGroups,
        int $createdAt,
        int $updatedAt,
        string $performableHtml,
        string $migratedFrom,
        bool $ignoreCurrentValues,
        array $metaData,
        bool $deletable,
        string $inlineMessage,
        string $tmsId,
        bool $captchaEnabled,
        string $campaignGuid,
        bool $cloneable,
        bool $editable,
        string $formType,
        int $deletedAt,
        string $themeName,
        int $parentId,
        string $style,
        bool $isPublished,
        int $publishAt,
        int $unpublishAt,
        array $multivariateTest,
        int $kickbackEmailWorkflowId,
        string $kickbackEmailsJson,
        string $customUid,
        bool $createMarketableContact
    )
    {
        $this->portalId = $portalId;
        $this->guid = $guid;
        $this->name = $name;
        $this->action = $action;
        $this->method = $method;
        $this->cssClass = $cssClass;
        $this->redirect = $redirect;
        $this->submitText = $submitText;
        $this->followUpId = $followUpId;
        $this->notifyRecipients = $notifyRecipients;
        $this->leadNurturingCampaignId = $leadNurturingCampaignId;
        $this->formFieldGroups = $formFieldGroups;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->performableHtml = $performableHtml;
        $this->migratedFrom = $migratedFrom;
        $this->ignoreCurrentValues = $ignoreCurrentValues;
        $this->metaData = $metaData;
        $this->deletable = $deletable;
        $this->inlineMessage = $inlineMessage;
        $this->tmsId = $tmsId;
        $this->captchaEnabled = $captchaEnabled;
        $this->campaignGuid = $campaignGuid;
        $this->cloneable = $cloneable;
        $this->editable = $editable;
        $this->formType = $formType;
        $this->deletedAt = $deletedAt;
        $this->themeName = $themeName;
        $this->parentId = $parentId;
        $this->style = $style;
        $this->isPublished = $isPublished;
        $this->publishAt = $publishAt;
        $this->unpublishAt = $unpublishAt;
        $this->multivariateTest = $multivariateTest;
        $this->kickbackEmailWorkflowId = $kickbackEmailWorkflowId;
        $this->kickbackEmailsJson = $kickbackEmailsJson;
        $this->customUid = $customUid;
        $this->createMarketableContact = $createMarketableContact;
    }

    /**
     * @return int
     */
    public function getPortalId(): int
    {
        return $this->portalId;
    }

    /**
     * @return string
     */
    public function getGuid(): string
    {
        return $this->guid;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getCssClass(): string
    {
        return $this->cssClass;
    }

    /**
     * @return string
     */
    public function getRedirect(): string
    {
        return $this->redirect;
    }

    /**
     * @return string
     */
    public function getSubmitText(): string
    {
        return $this->submitText;
    }

    /**
     * @return string
     */
    public function getFollowUpId(): string
    {
        return $this->followUpId;
    }

    /**
     * @return string
     */
    public function getNotifyRecipients(): string
    {
        return $this->notifyRecipients;
    }

    /**
     * @return string
     */
    public function getLeadNurturingCampaignId(): string
    {
        return $this->leadNurturingCampaignId;
    }

    /**
     * @return array
     */
    public function getFormFieldGroups(): array
    {
        return $this->formFieldGroups;
    }

    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->createdAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt(): int
    {
        return $this->updatedAt;
    }

    /**
     * @return string
     */
    public function getPerformableHtml(): string
    {
        return $this->performableHtml;
    }

    /**
     * @return string
     */
    public function getMigratedFrom(): string
    {
        return $this->migratedFrom;
    }

    /**
     * @return bool
     */
    public function isIgnoreCurrentValues(): bool
    {
        return $this->ignoreCurrentValues;
    }

    /**
     * @return array
     */
    public function getMetaData(): array
    {
        return $this->metaData;
    }

    /**
     * @return bool
     */
    public function isDeletable(): bool
    {
        return $this->deletable;
    }

    /**
     * @return string
     */
    public function getInlineMessage(): string
    {
        return $this->inlineMessage;
    }

    /**
     * @return string
     */
    public function getTmsId(): string
    {
        return $this->tmsId;
    }

    /**
     * @return bool
     */
    public function isCaptchaEnabled(): bool
    {
        return $this->captchaEnabled;
    }

    /**
     * @return string
     */
    public function getCampaignGuid(): string
    {
        return $this->campaignGuid;
    }

    /**
     * @return bool
     */
    public function isCloneable(): bool
    {
        return $this->cloneable;
    }

    /**
     * @return bool
     */
    public function isEditable(): bool
    {
        return $this->editable;
    }

    /**
     * @return string
     */
    public function getFormType(): string
    {
        return $this->formType;
    }

    /**
     * @return int
     */
    public function getDeletedAt(): int
    {
        return $this->deletedAt;
    }

    /**
     * @return string
     */
    public function getThemeName(): string
    {
        return $this->themeName;
    }

    /**
     * @return int
     */
    public function getParentId(): int
    {
        return $this->parentId;
    }

    /**
     * @return string
     */
    public function getStyle(): string
    {
        return $this->style;
    }

    /**
     * @return bool
     */
    public function isPublished(): bool
    {
        return $this->isPublished;
    }

    /**
     * @return int
     */
    public function getPublishAt(): int
    {
        return $this->publishAt;
    }

    /**
     * @return int
     */
    public function getUnpublishAt(): int
    {
        return $this->unpublishAt;
    }

    /**
     * @return array
     */
    public function getMultivariateTest(): array
    {
        return $this->multivariateTest;
    }

    /**
     * @return int
     */
    public function getKickbackEmailWorkflowId(): int
    {
        return $this->kickbackEmailWorkflowId;
    }

    /**
     * @return string
     */
    public function getKickbackEmailsJson(): string
    {
        return $this->kickbackEmailsJson;
    }

    /**
     * @return string
     */
    public function getCustomUid(): string
    {
        return $this->customUid;
    }

    /**
     * @return bool
     */
    public function isCreateMarketableContact(): bool
    {
        return $this->createMarketableContact;
    }
}