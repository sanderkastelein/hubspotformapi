## HubSpot API

How to run:
- Copy the .env.example to .env
- Configure your HUBSPOT_API_KEY
- Run composer install
- Make sure you have a webserver running in the public/ folder you can use `php -S localhost:8080` to test locally.


###Resources
- HubSpot general API docs: https://developers.hubspot.com/docs/overview
- HubSpot form docs: https://developers.hubspot.com/docs/methods/forms/forms_overview